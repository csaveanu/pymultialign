"""
This script transforms a multiple fasta alignment in a similarity matrix.
The result can be further used for visualization, in which the level of conservation of a given residue
is computed on the basis of a multiple sequence alignment - however, the final output depends only on
two chosen sequences. The input file has to be in the FASTA format, with gaps shown as "-". All sequences
must have the same length.

Usage:

--alignment, -a = fasta multiple alignment (each sequence has equal length, '-' for gaps)
--uniqueid1, -u = unique identifier for one of the sequences, e.g. "scer"
--uniqueid2, -w = unique identifier for a second sequence, e.g. "hsap"
--output, -o = output file name with the results, usable in R, for example
--help, -h = this help message

Cosmin Saveanu, 2019
"""

import sys, getopt
from operator import itemgetter
import pandas as pd
from collections import Counter

#classes defined by code copied from Stack Overflow to work with FASTA files
class Prot:
    ''' Object representing a FASTA record. '''
    def __init__(self, header, sequence):
        self.head = header
        self.seq = sequence
    def __repr__(self):
        return '[HTML]' % (self.head)
    def __str__(self, separator=''):
        return '>%s\n%s' % (self.head, separator.join(self.seq))
    def __len__(self):
        return len(''.join(self.seq))
    @property
    def sequence(self, separator=''):
        return separator.join(self.seq)

class Fasta:
    ''' A FASTA iterator/generates Prot objects. '''
    def __init__(self, handle):
        self.handle = handle
    def __repr__(self):
        return '[HTML]' % (self.handle)
    def __iter__(self):
        header, sequence = '', []
        for line in self.handle:
            if line[0] == '>':
                if sequence: yield Prot(header, sequence)
                header = line[1:-1]
                sequence = []
            else:
                sequence.append(line.strip())
        yield Prot(header, sequence)
# do fasta = Fasta(handle) and then for record in fasta to yield Prot objects


#main
#__________________________________

#uniqueid_str1="scer"
#uniqueid_str2="hsap"
        
def get_positions_from_gapped(seqwithgaps):
    #recover a list of lists that has the same
    #length as the original sequence, but has the
    #position of the character in the string as the first
    #element and its real position in the "ungapped" sequence
    pos_myseq=[]
    ctr_true = 0
    ctr_align = 0
    for aa in seqwithgaps:
        if aa=="-":
            pos_myseq.append((ctr_align, 0))
        else:
            pos_myseq.append((ctr_align, ctr_true))
            ctr_true+=1
        ctr_align+=1
    return(pos_myseq)
    
def get_ungapped_positions(seqwithgaps):
    pos_myseq_short=[]
    ctr_true = 0
    ctr_align = 0
    for aa in seqwithgaps:
        if aa=="-":
            pass
        else:
            pos_myseq_short.append((ctr_align, ctr_true))
            ctr_true+=1
            #print(aa, ctr_true, ctr_align)
        ctr_align+=1
    return(pos_myseq_short)

def calculate_seq_percent(aligndata, normval):
    align_data_pc=[]
    for pos in aligndata:
        apos = pos[0]
        grcount = pos[2][1]
        grgroup = pos[2][0]
        ownpos = pos[1]
        ownaa = pos[3]
        if ownaa in grgroup:
            align_data_pc.append([apos, ownpos, grcount/normval*100, grgroup, ownaa])
        else:
            align_data_pc.append([apos, ownpos, 0.0, grgroup, ownaa])
    return(align_data_pc)

def main(argv=None):
    global alignment_fname, output_fname, uniqueid_str1, uniqueid_str2
    alignment_fname, output_fname, uniqueid_str1, uniqueid_str2 = "", "", "", ""
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "ha:u:w:o:", ["help",
                                       "alignment=", 
                                       "uniqueid1=",
                                       "uniqueid2=",
                                       "output="])
        except:
            print("Problem reading arguments")
        # option processing
        for option, value in opts:
            if option in ("-h", "--help"):
                print(__doc__)
            if option in ("-a", "--alignment"):
                alignment_fname= value
            if option in ("-u", "--uniqueid1="):
                uniqueid_str1 = value
            if option in ("-w", "--uniqueid2="):
                uniqueid_str2 = value
            if option in ("-o", "--output="):
                output_fname = value
                
        if (alignment_fname == "" or uniqueid_str1 == "" or uniqueid_str2 == "" or output_fname == ""):
            print("No input filename, output file name or missing parameter.")
            sys.exit(2)
        else:
            try:
                #read data
                print(alignment_fname, uniqueid_str1, uniqueid_str2, output_fname)
                print("Reading ", alignment_fname) 
                print("The results will be written to a file called ", output_fname)
                f=open(alignment_fname, "r")
                fasta=Fasta(f)
                align=[]
                for entry in fasta:
                    align.append(entry)
                f.close()
                
                #put all the data in a matrix
                #we assume that all the sequences have equal length, with the included gaps
                proteinsequences_list = [list(prot.sequence) for prot in align]
                #obtain first a list of all the sequences from the Fasta object
                poslist = [list(pos) for pos in zip(*proteinsequences_list)]
                #use the zip function to put together of the aminoacids at each position of the alignment
                counts_per_position = [Counter(".".join(aa_at_pos)) for aa_at_pos in poslist]
                #create a list of objects that contain the counts for each aminoacid at each position
                #Counter calls give back 0, if there was no aminoacid - like .get(aa, 0) on dict

                res_summary=[]
                similaraa=["GAVLI", "FYW", "CM", "ST", "KRH", "DENQ", "P", "-"]
                for position in counts_per_position:
                    sgroup_d={sgroup:0 for sgroup in similaraa}
                    #initialize a dictionary with the groups of aminoacids as keys
                    for simgroup in sgroup_d.keys():
                        for aa in position.keys():
                            #print(aa, simgroup)
                            if aa in simgroup:
                                sgroup_d[simgroup]=sgroup_d[simgroup]+position[aa]
                    res_summary.append(sgroup_d)
                

                sorted_res=[]
                for pos in res_summary:
                    sorted_res.append(sorted(pos.items(), key=itemgetter(1), reverse=True))
                #res_summary has now tuples: "aagroup" , number of proteins in alignment
                
                #find where the sequence of interest is
                for protein in align:
                    if uniqueid_str1 in protein.head:
                        print(protein.head)
                        myseq1=protein.sequence
                for protein in align:
                    if uniqueid_str2 in protein.head:
                        print(protein.head)
                        myseq2=protein.sequence
                #create a list of coordinates that correspond to actual aminoacids
                #the list has many gaps (correspond to insertions)
                #remove all positions that do not correspond to current protein
                pos_myseq1_short = get_ungapped_positions(myseq1)
                pos_myseq2_short = get_ungapped_positions(myseq2)
                #remove the "-" from the sequence of interest
                trtable = str.maketrans(dict.fromkeys('-'))
                myseq1_noX = myseq1.translate(trtable)
                myseq2_noX = myseq2.translate(trtable)

                align_data_specific1=[]
                for pos in pos_myseq1_short:
                    alignpos=pos[0]
                    singlepos=pos[1]
                    align_data_specific1.append([alignpos, singlepos, sorted_res[alignpos][0], myseq1_noX[singlepos]])
                
                align_data_specific2=[]
                for pos in pos_myseq2_short:
                    alignpos=pos[0]
                    singlepos=pos[1]
                    align_data_specific2.append([alignpos, singlepos, sorted_res[alignpos][0], myseq2_noX[singlepos]])

                normval = len(align)
                                
                align_data_pc1=calculate_seq_percent(align_data_specific1, normval)
                align_data_pc2=calculate_seq_percent(align_data_specific2, normval)
                #example of entries: initial number, index in sequence, percent similarity, most represented group, own amino acid at position
                #[45, 0, 49.45, 'CM', 'M']
                #[46, 1, 0.0, '-', 'S']
                
                dt1 = pd.DataFrame(align_data_pc1)
                dt1.columns=["alignment_pos", "position", "pcaligned", "aagroup", "ownaa"]
                dt1["position"]=dt1["position"]+1
                dt1["alignment_pos"]=dt1["alignment_pos"]+1
                
                dt2 =  pd.DataFrame(align_data_pc2)
                dt2.columns=["alignment_pos", "position", "pcaligned", "aagroup", "ownaa"]
                dt2["position"]=dt2["position"]+1
                dt2["alignment_pos"]=dt2["alignment_pos"]+1

                dttwo = pd.merge(dt1, dt2, how='outer', on="alignment_pos",
                                 sort=True,
                                 suffixes=("_1", "_2"))
                #dttwo.info()
                #dttwo["position_1"] = dttwo["position_1"].astype('Int64')
                dttwo['idx']=dttwo.index.values+1
                #write the results as a table, but format first the percent values to include a single decimal digit
                #dttwo['pcaligned_1'] = ["{:.1f}".format(vlue) for vlue in dttwo['pcaligned_1']]
                #dttwo['pcaligned_2'] = ["{:.1f}".format(vlue) for vlue in dttwo['pcaligned_2']]
                dttwo.to_csv(output_fname, sep='\t', index=False, float_format="%.0f")
                #using the float_format is important because positions became float with pd.merge (NaN is coded as float...)

            finally:
                pass
                
    except:
        print("\t No arguments provided.")
        print("\t for help use --help")
        return 2    
    
if __name__ == "__main__":
    sys.exit(main())
