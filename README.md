# pymultialign

Two scripts to isolate two sequences from a multiple alignment and show them side by side. The result can be useful to:

1. find corresponding residue numbers between the aligned sequences;
2. compress information about similarity in a graph.

Example of use:

```
python multiplalign_to_matrix.py -a testdata/DCP2_MAFFT.mfa -u "YEASX" -w "SCHPM" -o testdata/DCP2_MAFFT_matrix.txt
python matrix_to_graph_alignment.py -m testdata/DCP2_MAFFT_matrix.txt -o testdata/Scer_vs_Spom_DCP2.pdf
```
From a multiple alignment (in jalview):

<!-- ![jalview region of alignment](images/screenshot_jalview_dcp2.png) -->
<img src="images/screenshot_jalview_dcp2.png" alt="jalview region of alignment" width="600">

one can get the following image, with only two of the sequences highlighted, lower is S. cerevisiae, upper is S. pombe:

![example of graphical output](testdata/Scer_vs_Spom_DCP2.png)

More information in the pymultialign_description.md file.